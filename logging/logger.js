const moment = require('moment');
const winston = require("winston");

const logger = winston.createLogger({
    level: process.env.LOG_LEVEL || "debug",
    format: winston.format.combine(
        winston.format.timestamp({
           format: `${moment().format()}`
       }),
       winston.format.printf(info => `${info.level}: ${[info.timestamp]}: ${info.message}`),
    ),
    transports: [new winston.transports.File({
        filename: 'logging/Logging.log'
    })],
  });

module.exports = logger;