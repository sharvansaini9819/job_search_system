const morgan = require("morgan");
const logger = require('../logging/logger');
const stream = {
  write: (message) => logger.http(message),
};
const skip = () => {
  const env = process.env.NODE_ENV || "development";
  return env !== "development";
};
const morganMiddleware = morgan(
    ":method http://localhost/5001:url responseCode = :status",
  { stream, skip }
);
module.exports = morganMiddleware;