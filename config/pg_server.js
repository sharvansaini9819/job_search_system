const fs = require('fs');
const pgp = require('pg-promise')();

const rawdata = fs.readFileSync('database.json');
const dbConfig = JSON.parse(rawdata).dev;

const db = pgp({
  user: dbConfig.user,
  password: dbConfig.password,
  host: dbConfig.host,
  database: dbConfig.database
});

module.exports = db;

