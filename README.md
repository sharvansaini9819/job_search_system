# Job_Search_System
GitLab URL : https://gitlab.com/sharvansaini9819/job_search_system.git

## Problem Statement

Rest API which allows user to register/login and search for the jobs in Job Search System. It also allows admin registeration/login to create and delete the jobs from Job Search Sytem.

### Functionality

- Rest APIs for enabling admin and user sign-in using email password
- It will allow admins to create and delete jobs
- Filtering of jobs based on user input parameters
- Security using JWT
- Error handling along with logging
- Testing using thunderclient
- Documentation using markdown
- Databse used Postgresql
- Dataset from kaggle

## Database
- Postgresql databse is integrated to save data and perform the CRUD operations
- Connected to Pg Admin using pg-promise and configurations mentioned in databse.json file
    ```
    Path - ./database.json
    ```
- To create sql files and migrate them to database using below command
    ```
    npm install -g db-migrate-pg
    db-migrate create initialize --sql-file
    ```
- Before running the project run the below command to run all the sql files and set up the database
    ```
    db-migrate up initialize
    ```

## Tables used
- user - It is used for registration and login
    ```
    {
        "email":"test@gmail.com",
        "username": "test",
        "password": "test",
        "role_id": 1                  
    }
    ```
- roles - It maps the role_id to role_name
    ```
    {
        "role_id": 1,
        "role_name": "admin"               
    }
    ```
- job_details - It contains all the job details
    ```
    [
        {
            "job_title": "Shift Manager",
            "job_ID": 3471657636,
            "job_type": "Remote",
            "location": "Mission Hills, CA 91345",
            "city": "Mission Hills",
            "state": "CA",
            "country": "United States",
            "compnay_name": "Del Taco LLC"
        },
        {
            "job_title": "Senior UI Software Developer",
            "job_ID": 3467387080,
            "job_type": "Onsite",
            "location": "Charlotte, NC 28210",
            "city": "Charlotte",
            "state": "NC",
            "country": "United States",
            "compnay_name": "PeraHealth"
        }
    ]
    ```
- user_job_apply - It tracks the jobs applied by user
    ```
    {
        "job_id":[50093206,88232570]
        "email": "abc@gmail.com"
    }
    ```


## Code Reference

- ./app.js - main class which connects with server and has 3 routes

    - Route for admin and user registration and login
    ```
    ./authentication/user_registration_login
    ```
    - Route for job details
    ```    
    ./routes/jobDetails
    ```
    -  Route for job applying
    ```
    ./routes/userJobApply
    ```
---
- Authentication/Authorization using JWT
    - It authenticates a member based on email, role_id(admin/user) and secret key
    - Authentication middleware is used to check if member is authentic only then the token is generated using the login api
    - Password is encoded using bycrpt before saving into db for security purpose
    ```
    POST API - localhost:5001/login
    ```
    - Middleware also checks if the member is authorized to access a particular api such as admin members are only allowed to create or delete a job


---
- Registration/Login API
    - It allows user to register himself and save user details in users table
    ```
    POST API - localhost:5001/register
    Request Body Sample                 (role - admin/user)
    {
        "email":"test@gmail.com",
        "username": "test",
        "password": "test",
        "role": "user"                  
    }
    ```
    - It allows user to login and generate token for authorization using email and paaword
    ```
    POST API - localhost:5001/login
    Request Body Sample
    {
        "email":"test@gmail.com",
        "password": "test"
    }
    ```
---
- Job Details API 
    - It will allow admin and authorized user to fetch data from job_details table based on any parameter
    ```
    GET API - localhost:5001/api/job?job_id=60684395
    ```
    - Allows admin to create new job and save in job_details table
    ```
    POST API - localhost:5001/api/job
    Request Body Sample                 (job_id - PRIMARY KEY)
    {
	    "job_title": "abc",
	    "job_id": "9999999999",         
        "job_type": "abc",
        "location": "abc",
        "city": "abc",
        "state": "ABC",
        "country": "abc",
        "compnay_name": "abc"
    }
    ```
    - Allows admin to delete an existing job from job_details table based on job_id
    ```
    DELETE API - localhost:5001/api/job?job_id=9999999999
    ```
---
- Job Apply API
    - It fetches the details of all jobs applied from user_job_apply table using users email
    ```
    GET API - localhost:5001/api/user/apply
    ```
    - It saves the job_id and email of the user in user_job_apply table
    ```
    POST API - localhost:5001/api/user/apply
    Request Body Sample
    {
        "job_id":[50093206,88232570]
    }
    ```

## Logging
- Winston library is used for logging different levels of logs and saving them in Logging.log file
- Morgan middleware is used in combination with wiston to track the http requests being made to server and saving them in log file
    ```
    Logging.log file sample

    debug: 2023-08-18T12:42:21+05:30: Role name : user
    debug: 2023-08-18T12:42:21+05:30: job_Ids : 50093206,88232570
    info: 2023-08-18T12:42:21+05:30: Applied Jobs fetched successfully
    http: 2023-08-18T12:42:21+05:30: GET http://localhost/5001/api/user/apply responseCode = 200
    ```

## Testing
- chai and mocha library are used for testing the rest api using thunderclient
## Team Members

- Sharvan Saini
- Kirti Goel