const express = require('express');
const app = express();
const PORT = 5001;
const morganMiddleware = require('./logging/morgan_middleware');
app.use(morganMiddleware);

app.use(express.json())

// Route for registration and login
app.use('/',require('./authentication/user_registration_login'))

// Route for job details
app.use('/api/job', require('./routes/jobDetails'))

// Route for job applying
app.use('/api/user/apply', require('./routes/userJobApply'))

// start the server
app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
