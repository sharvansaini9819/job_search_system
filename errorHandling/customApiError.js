class customApiError extends Error {
    constructor(statusCode, message) {
        super(message);
        this.statusCode = statusCode;
        this.name = "customApiError";
    }
}

module.exports = customApiError