const chai = require('chai')
let { expect } = require('chai')
let chaiHttp = require('chai-http')
chai.use(chaiHttp)
const app = require('../app')
let appURL = "http://localhost:5001"

describe('User Registration API', function () {

    let userToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAZ21haWwuY29tIiwicm9sZSI6MiwiaWF0IjoxNjkyMjY5OTY5fQ.4gW3KjEVKt_wrSudeD3eH5F-i9oaZW9VTnIXYGrcM8g'
    
    //Test Case - 1
    it('GET /api/user/apply - Successfully fetch job details based on user email', async function () {
        let res = await chai.request(appURL)
        .get('/api/user/apply').set('authorization', 'Bearer ' + userToken)
        expect(res).to.have.status(200)
    })

    //Test Case - 2
    it('GET /api/user/apply - Invalid api url', async function () {
        let res = await chai.request(appURL)
        .get('/api/user/appies').set('authorization', 'Bearer abc')
        expect(res).to.have.status(404)
    })

    //Test Case - 3
    it('POST /api/user/apply - Insert job ids successfully', async function () {
        let job_Ids = {
            "job_id":[50093206,88232570]
          }
        let res = await chai.request(appURL)
        .post('/api/user/apply').set('authorization', 'Bearer ' + userToken).send(job_Ids)
        expect(res).to.have.status(200)
    })
});