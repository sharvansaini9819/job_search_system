const chai = require('chai')
let { expect } = require('chai')
let chaiHttp = require('chai-http')
chai.use(chaiHttp)
const app = require('../app')
let appURL = "http://localhost:5001"

describe('jobDetailsAPI', function () {

    let adminToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFiY0BnbWFpbC5jb20iLCJyb2xlIjoxLCJpYXQiOjE2OTIyNTkwMDV9.K-449Y1Fi4-ZgLa0ADqcBM84L42E4eWl1HyMz1ZpCCE'
    let job_id = 9999999999
    
    //Test Case - 1
    it('GET /api/job - It should return all job details', async function () {
        let res = await chai.request(appURL)
        .get('/api/job').set('authorization', 'Bearer ' + adminToken)
        expect(res).to.have.status(200)
    })

    //Test Case - 2
    it(`GET /api/job - Invalid job id`, async function () {
        let res = await chai.request(appURL+"/api/job")
        .get('/api/job?job_id='+job_id).set('authorization', 'Bearer ' + adminToken)
        expect(res).to.have.status(404)
    })

    //Test Case - 3
    it(`POST /api/job - It should successfully insert`, async function () {
        let job = {
            "job_title": "abc",
            "job_id": 9999999999,
            "job_type": "abc",
            "location": "abc",
            "city": "abc",
            "state": "ABC",
            "country": "abc",
            "compnay_name": "abc"
        }
        let res = await chai.request(appURL)
        .post('/api/job').set('authorization', 'Bearer ' + adminToken).send(job)
        expect(res).to.have.status(200)
    })
    
    //Test Case - 4
    it(`POST /api/job - It should fail as job_id not present `, async function () {
        let job = {
            "job_title": "abc",
            "job_type": "abc",
            "location": "abc",
            "city": "abc",
            "state": "ABC",
            "country": "abc",
            "compnay_name": "abc"
        }
        let res = await chai.request(appURL)
        .post('/api/job').set('authorization', 'Bearer ' + adminToken).send(job)
        expect(res).to.have.status(500)
    })
    
    //Test Case - 5
    it(`DELETE /api/job - It should delete job successfully `, async function () {
        let res = await chai.request(appURL)
        .delete('/api/job?job_id='+job_id).set('authorization', 'Bearer ' + adminToken)
        expect(res).to.have.status(200)
    })
    
    //Test Case - 6
    it(`DELETE /api/job - Not valid job id present to be deleted `, async function () {
        let res = await chai.request(appURL)
        .delete('/api/job?job_id='+job_id).set('authorization', 'Bearer ' + adminToken)
        expect(res).to.have.status(400)
    })
})