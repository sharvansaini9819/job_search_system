const chai = require('chai')
let { expect } = require('chai')
let chaiHttp = require('chai-http')
chai.use(chaiHttp)
const app = require('../app')
let appURL = "http://localhost:5001"

describe('User Registration API', function () {

    let registerData = {
        "email":"abc1@gmail.com",
        "username": "abc1",
        "password": "abc1",
        "role": "admin"
    }

    //Test Case - 1
    it('POST /register - It should successfully register user', async function () {
        let res = await chai.request(appURL)
        .post('/register').send(registerData)
        expect(res).to.have.status(200)
    })

    //Test Case - 2
    it('POST /register - User is already registered', async function () {
        let res = await chai.request(appURL)
        .post('/register').send(registerData)
        expect(res).to.have.status(500)
    })

    //Test Case - 3
    it('POST /login - User successfully logged-in', async function () {
        let loginData = {
            "email":"abc@gmail.com",
            "password": "abc"
        }
        let res = await chai.request(appURL)
        .post('/login').send(loginData)
        expect(res).to.have.status(200)
    })

    //Test Case - 4
    it('POST /login - User unable to login - Unauthorized user', async function () {
        let loginData = {
            "email":"xyz@gmail.com",
            "password": "xyz"
        }
        let res = await chai.request(appURL)
        .post('/login').send(loginData)
        expect(res).to.have.status(401)
    })
})