const jwt = require('jsonwebtoken');
const secretKey = 'your-secret-key';
const db = require('../config/pg_server');
const customApiError = require('../errorHandling/customApiError')
const logger = require('../logging/logger');

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];

    if (token == null) {
        let customError = new customApiError(401, 'Unauthorized Member')
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }

    jwt.verify(token, secretKey, async (err, user) => {
        if (err){
            let customError = new customApiError(401, 'Unauthorized Member')
            logger.error(customError.message)
            return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
        }
        req.user = user;
        next();
    });
}

 function authorizeRole(roleName) {
    return async (req, res, next) => {
        const email = req.user.email;
        const userRole = req.user.role;
        try {
            const role =  await db.one('SELECT * FROM roles WHERE id = $1', [userRole]);
            logger.debug('Role name : '+roleName)
            if (role.name === roleName) {
                next();
            } else {
                let customError = new customApiError(401, 'Unauthorized Member')
                logger.error(customError.message)
                return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
            }
        } catch (error) {
            let customError = new customApiError(401, 'Unauthorized Member')
            logger.error(customError.message)
            return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
        }
    };
}

module.exports = {
    authenticateToken,
    authorizeRole,
};
