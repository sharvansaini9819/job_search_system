const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const db = require('../config/pg_server');
const logger = require('../logging/logger');
const customApiError = require('../errorHandling/customApiError')
const validator = require('validator');
const secretKey = 'your-secret-key';

// POST API to register a user and save user details in users table
router.post('/register', async (req, res) => {
    let data = req.body;
    const hashedPassword = await bcrypt.hash(data.password, 10);
    try {
        if(validator.isEmail(data.email)){
            const roleId = await db.one('SELECT id FROM roles WHERE name = $1', [data.role]);
            let insertUserSqlQuery = 'INSERT INTO users (email,username, password, role_id) VALUES ($1, $2, $3, $4) RETURNING email';
            const newUser = await db.one(
                insertUserSqlQuery,
                [data.email, data.username, hashedPassword, roleId.id]
            );
            res.json({ statusCode: 200, message: 'User registered successfully', email: newUser.email })
        }else{
            let customError = new customApiError(400, 'Invalid email address')
            logger.error(customError.message)
            return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message }) 
        }
        
    } catch (error) {
        let customError = new customApiError(500, error.message)
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }
});

// POST API to generate token for the registered users
router.post('/login', async (req, res) => {
    const { email, password } = req.body;
    try {
        const user = await db.one('SELECT * FROM users WHERE email = $1', [email]);
        const passwordMatch = await bcrypt.compare(password, user.password);
        if (passwordMatch) {
            const token = jwt.sign({ email: user.email, role: user.role_id }, secretKey);
            logger.info('Token generated successfully')
            res.json({ statusCode: 200, token })
        } else {
            let customError = new customApiError(401, 'Authentication failed')
            logger.error(customError.message)
            return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
        }
    } catch (error) {
        let customError = new customApiError(401, 'Authentication failed')
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }
});

module.exports = router