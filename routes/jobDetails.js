const express = require('express')
const router = express.Router()
const db = require('../config/pg_server')
const customApiError = require('../errorHandling/customApiError')
const lodash = require('lodash')
const { authenticateToken, authorizeRole } = require('../authentication/auth_middleware')
const logger = require('../logging/logger');

// GET API call to fetch data from job_details table
router.get('/', authenticateToken, async (req, res) => {
    try {
        let sql = 'SELECT * FROM job_details WHERE'
        for(let key in req.query){
            if(key == 'job_title'){
                sql += ` ${key} like '%${req.query[key]}%' AND `
            } else{
                sql += ` ${key} = '${req.query[key]}' AND `
            }
        }
        sql = sql.slice(0,-5)
        logger.debug(sql)
        let result = await db.any(sql)
        if(!lodash.isEmpty(result)){
            logger.info('data successfully found')
            res.json({ statusCode: 200, message: 'data successfully found', result})
        } else{
            let customError = new customApiError(404, 'No data found')
            logger.error(customError.message)
            return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
        }        
    } catch (error) {
        let customError = new customApiError(400, error.message)
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }
});

// POST API call to insert data in job_details table
router.post('/',authenticateToken,authorizeRole('admin'), async (req, res) => {
    let data = req.body;
    try {
        const newJob = await db.one(
            `INSERT INTO job_details (job_title, job_id, job_type, location, city, state, country, compnay_name) 
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8) RETURNING job_id`,
            [data.job_title, data.job_id, data.job_type, data.location, data.city, data.state, data.country, data.compnay_name]
        );
        let msg = 'New job created successfully for job_id : '+newJob.job_id
        logger.info(msg)
        res.json({ statusCode:200, message: msg });
    } catch (error) {
        let customError = new customApiError(500, error.message)
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }
});

// DELETE API call to delete data on basis of job_id from job_details table
router.delete('/', authenticateToken,authorizeRole('admin'),async (req, res) => {
    try {
        const deleteJob = await db.oneOrNone(
            'delete from job_details where job_id = $1 RETURNING job_id',
            [req.query.job_id]
        );
        if(deleteJob){
            let msg = 'Job deleted successfully for job_id : '+deleteJob.job_id
            logger.info(msg)
            res.json({ statusCode:200, message: msg });
        } else{
            let msg = 'No data found for job_id : '+req.query.job_id
            let customError = new customApiError(400, msg)
            logger.error(customError.message)
            return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
        }
    } catch (error) {
        let customError = new customApiError(500, error.message)
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }
});

module.exports = router;