const express = require('express')
const router = express.Router()
const db = require('../config/pg_server')
const logger = require('../logging/logger');
const customApiError = require('../errorHandling/customApiError')
const { authenticateToken, authorizeRole } = require('../authentication/auth_middleware');

// GET API to fetch the details of all jobs applied by the user from user_job_apply table
router.get('/', authenticateToken, authorizeRole('user'), async (req, res) => {
    try {
        const job_Ids = await db.one('select job_id from user_job_apply where email=$1', [req.user.email]);
        logger.debug('job_Ids : ' + job_Ids.job_id)
        const appliedJobIds = job_Ids.job_id.map(str => parseInt(str, 10));
        const appliedJobs = await db.any('select * from job_details where job_id in ($1:csv)', [appliedJobIds]);
        logger.info('Applied Jobs fetched successfully')
        res.json({ statusCode: 200, message: 'Applied Jobs', appliedJobs: appliedJobs })
    } catch (error) {
        let customError = new customApiError(500, error.message)
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }
});

// POST API to insert job_id and email of the user in user_job_apply table
router.post('/', authenticateToken, authorizeRole('user'), async (req, res) => {
    let data = req.body;
    try {
        logger.debug('Job_id : ' + data.job_id)
        logger.debug('User email : ' + req.user.email)
        const userAppliedJob = await db.oneOrNone('SELECT * FROM user_job_apply WHERE email = $1', [req.user.email]);
        if (userAppliedJob) {
            const appliedJobIds = userAppliedJob.job_id.map(str => parseInt(str, 10));
            const updateAppliedJobIds = [...new Set(data.job_id.concat(appliedJobIds))];
            const jobApply = await db.result(
                `update user_job_apply set job_id=$1 where email=$2`, [updateAppliedJobIds, req.user.email]);

            let msg = 'Successfully job applied by ' + req.user.email;
            logger.info(msg)
            res.json({ statusCode: 200, message: msg })
        } else {
            const jobApply = await db.one(
                `Insert into user_job_apply (email,job_id) values($1,$2) RETURNING email`,
                [req.user.email, data.job_id]);

            let msg = 'Successfully job applied by ' + jobApply.email;
            logger.info(msg)
            res.json({ statusCode: 200, message: msg })
        }
    } catch (error) {
        let customError = new customApiError(500, error.message)
        logger.error(customError.message)
        return res.status(customError.statusCode).json({ statusCode: customError.statusCode, error: customError.message })
    }
});

module.exports = router;